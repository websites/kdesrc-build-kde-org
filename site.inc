<?php
  $templatepath = "aether/";

  $site = 'kdesrc-build';
  $site_title = "kdesrc-build - KDE Build Tool";
  $site_external = true;
  $site_logo_left = siteLogo("kdesrc-build-logo_64.png", "64", "64");
  $site_search = false;
  $site_menus = 2;

  $current_version = '-git';

  $name = "kdesrc-build.kde.org Webmaster";
  $mail = '&#109;&#112;&#121;&#110;&#101;&#64;&#107;&#100;&#101;&#46;&#111;&#114;&#103;';

  // Doesn't seem to work.
  $showedit = false;
?>
