<?php
  $page_title = "Subversion checkout mirrors";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

This page used to contain a grouping of Subversion module snapshots. Such snapshots
are no longer provided by this site, since the KDE project is switching most of its
source code hosting to a <a href="http://git-scm.org/">Git</a>-based solution at
<a href="http://git.kde.org/">http://git.kde.org/</a>

<?php
  include("footer.inc");
?>
