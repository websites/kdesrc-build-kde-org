<?php
  $page_title = "Syntax highlighting for KWrite/Kate";
  $site_root = "../";
  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>I have made a syntax highlighting file for Kate and KWrite that will give you folding
and highlighting support for the kdesrc-buildrc files.</p>

<p>This highlighting file is part of the default Kate installation.</p>

<p>You can enable highlighting for kdesrc-buildrc in the menu Tools->Highlighting->Configuration</p>

<center>
<img alt="Screenshot of syntax highlighting file in action"
     src="kwrite-syntax.png" />
</center>

<?php
  include("footer.inc");
?>
