<?php
  $site_root = '../';
  include("header.inc");
?>

<h1>Documentation</h1>

The official documentation for <b>kdesrc-build</b> is maintained at the
<a href="https://docs.kde.org/index.php?application=kdesrc-build&language=en">KDE
Documentation Site</a>.

You may follow <a
href="https://docs.kde.org/index.php?application=kdesrc-build&language=en">this
(direct) link to the latest version of documentation</a>.
Please update your bookmarks if needed, otherwise you will be redirected.

  <script>
    function redirect() {
      top.location = 'https://docs.kde.org/index.php?application=kdesrc-build&language=en';
    }
    setTimeout(redirect, 3000);
  </script>

<?php
  include("footer.inc");
?>
