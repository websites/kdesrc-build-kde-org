<?php
  $release_version = '1.15';
  $release_title = "kdesrc-build $release_version";
  $release_file = "kdesrc-build-$release_version.tar.bz2";
  $page_title = "kdesrc-build release $release_version";
  $site_root = "../";

  include("header.inc");
?>

<p>Released: <b>2012-May-05</b></p>

<p>Download it:
<a href="http://download.kde.org/stable/kdesrc-build/1.15/src/kdesrc-build-1.15.tar.bz2"><?php echo $release_file; ?></a>
-
<a href="http://download.kde.org/stable/kdesrc-build/1.15/src/kdesrc-build-1.15.tar.bz2.mirrorlist">Details (incl. PGP signature)</a>
</p>

<p><?php echo $release_title; ?> had 180 commits from 7 authors from the prior
release (<a href="kdesrc-build-1.14.1.php">1.14.1</a>). The changes are summarized
below:</p>

<h3>Summary of new features/changes:</h3>
<ul>

<li><p>kdesrc-build supports reordering modules from the
KDE project database so that they are built in the right order. This applies
even for modules that are automatically included from a group specified by the
user (e.g. if you asked to build all of kdeedu). <b>This relies on accurate
dependency information</b>, which is downloaded from the 
<code>kde-build-metadata</code>
repository automatically.</p></li>

<li><p>It is possible to download and build translation packages again (the
kde-languages option).
</p></li>

<li><p>kdesrc-build doesn't re-download the KDE Project database if it hasn't
changed.
</p></li>

<li><p>A large amount of refactoring has been performed (with more to follow).
</p></li>

</ul>

<hr/>

The full list of changes follows:

<h3>Bugfixes:</h3>
<ul>
<li>The Subversion modules <code>kde-base-artwork</code> and <code>kde-wallpapers</code> have the KDE/
prefix added like the other KDE Subversion modules. (Fix by Andre Woebbeking)</li>
<li>Documentation improvements from Burkhard Lück.</li>
<li>Fix module build order for kde-baseapps. kde-baseapps must be built before
konsole and kate due to a change in the logical/virtual project layout.
Thanks to David Faure for tracking down the cause and providing an
initial fix. (<a href="https://bugs.kde.org/show_bug.cgi?id=283127">Bug 283127</a>)</li>
<li>Similarly, build kactivities before kde-runtime. Noticed by Jan Hackel.
(<a href="https://bugs.kde.org/show_bug.cgi?id=298401">Bug 298401</a>)</li>
<li>Fix crash present when using <a href="/documentation/conf-options-table.html#conf-use-idle-io-priority">use-idle-io-priority</a>. (<a href="https://bugs.kde.org/show_bug.cgi?id=283071">Bug 283071</a>)</li>
<li>Do not force a KDE Project database update when using <a href="/documentation/supported-cmdline-params.html#cmdline-no-src">--no-src</a>. (<a href="https://bugs.kde.org/show_bug.cgi?id=283026">Bug 283026</a>)</li>
<li>Look for <code>dialog(1)</code> before trying to use it in <code>kdesrc-build-setup</code>. (<a href="https://bugs.kde.org/show_bug.cgi?id=290252">Bug 290252</a>)</li>
<li>Re-implement the broken l10n-building support. (<a href="https://bugs.kde.org/show_bug.cgi?id=283027">Bug 283027</a>)</li>
<li>Do not add entries to the environment twice (not a regression as a different
bug prevented this behavior from showing in earlier releases) (<a href="https://bugs.kde.org/show_bug.cgi?id=291357">Bug 291357</a>)</li>
<li>Ensure that specific module options override module-set options. This broke
in a previous release at some point. (<a href="https://bugs.kde.org/show_bug.cgi?id=288611">Bug 288611</a>)</li>
<li>Support <a href="/documentation/supported-cmdline-params.html#cmdline-resume-from">--resume-from</a> and <a href="/documentation/supported-cmdline-params.html#cmdline-resume-after">--resume-after</a> for <code>kde-projects</code> as well. (<a href="https://bugs.kde.org/show_bug.cgi?id=290450">Bug 290450</a>)</li>
<li>Move lockfile to where its associated rc-file is. This keeps kdesrc-build from
needlessly forbidding concurrent execution (you still must ensure your separate
rc-files do not themselves trample on each others' directories). (<a href="https://bugs.kde.org/show_bug.cgi?id=292266">Bug 292266</a>)</li>
<li>Fix handling of the KDE Project database on the very first run of kdesrc-build
if using <a href="/documentation/supported-cmdline-params.html#cmdline-pretend">--pretend</a>. (<a href="https://bugs.kde.org/show_bug.cgi?id=295715">Bug 295715</a>)</li>
<li>Updates/corrections to default branches of some modules (e.g. <code>qt</code>, <code>kdelibs</code>).
Thanks to David Faure and Eike Hein for watching these.</li>
<li>Properly show number of failed tests (David Faure).</li>
<li>New clones of KDE Project modules use the <code>kde:</code> prefix by default. Likewise
for git snapshots of KDE Project modules.</li>
<li>Fix XML parsing of the KDE Project module database (char handlers can be
called multiple times for a given tag). Thanks to Ralf Jung for the report
and detailed diagnosis.</li>
<li>Corrected repo change warning message. It does not actually currently try
to fix the problem it notes (but maybe someday...)</li>
<li>Process exit handlers are now <em>really</em> only run once (this should fix
receiving duplicated "Script finished at..." messages when using ^C to
stop execution).</li>
<li>Save persistent data in correct location when reading configuration files
from a relative path instead of an absolute path.</li>
<li>Install data to correct spot if KDE is not installed (noted by teprrr on IRC).</li>
<li>Improvements when running kdesrc-build for the first time and using
<a href="/documentation/supported-cmdline-params.html#cmdline-pretend">--pretend</a>.</li>
<li>Verbose progress output is only forced for <a href="http://qt-project.org/" title="Qt Project">Qt</a> when downloading from
<a href="http://gitorious.org/">Gitorious</a> as the KDE Git infrastructure does not
suffer from server-side disconnections on very large clones.</li>
<li>References to the old <code>apply-patches</code> option removed.</li>
<li>kdesrc-build fails much sooner if unable to change directory instead of
waiting for a later (usually unrelated) error.</li>
<li>The persistent <code>last-install-rev</code> entry is unset if kdesrc-build is used
to uninstall a module.</li>
<li>The error message for duplicate module names in a <a href="/documentation/conf-options-table.html#conf-use-modules">use-modules</a> entry is
no longer hardcoded to "kdelibs".</li>
<li>kdesrc-build-setup improvements from Raphael Kubo da Costa.</li>
</ul>

<h3>New features:</h3>

<ul>
<li>You can specify which SSH identity to request from the SSH Agent, using the
new <a href="/documentation/conf-options-table.html#conf-ssh-identity-file">ssh-identity-file</a> option. Patch provided by Ralf Jung. (<a href="https://bugs.kde.org/show_bug.cgi?id=284071">Bug 284071</a>)</li>
<li>Support KDE-provided built metadata. Specifically this allows kdesrc-build to
automatically build some modules in the right order (e.g. if you just ask to
build <code>kdevelop</code>, kdesrc-build has to actually build <code>kdevelop/kdevplatform</code>
before the rest of <code>kdevelop/*</code>). In addition this allows kdesrc-build to
ignore modules that should not be attempted to build but are present in the
KDE project database (e.g. some -www modules). (<a href="https://bugs.kde.org/show_bug.cgi?id=288378">Bug 288378</a>)</li>
<li>Unknown modules on the command line are assumed to be KDE Project modules
(kdesrc-build will still abort if it turns out there is no such module in the
KDE Project database). This can allow for quick and easy "one-shot"
installation of programs available in the KDE Project database.</li>
<li>The module used to build <a href="http://qt-project.org/" title="Qt Project">Qt</a> no longer must be
called "<code>qt-copy</code>". The sample configuration file uses <code>qt</code>. It is not
required to rename exsting modules. This change will pave the way for Qt 5
support to come (where Qt exists amongst multiple independent modules).
(<a href="https://bugs.kde.org/show_bug.cgi?id=265255">Bug 265255</a>)</li>
<li>The <code>qmake</code> build system is supported.</li>
<li>Allow specifying the location of the persistent data store (using <a href="/documentation/conf-options-table.html#conf-persistent-data-file">persistent-data-file</a>).
(<a href="https://bugs.kde.org/show_bug.cgi?id=223334">Bug 223334</a>)</li>
<li>kdesrc-build searches for "essential" programs before even bothering to start
the build. (<a href="https://bugs.kde.org/show_bug.cgi?id=263936">Bug 263936</a>)</li>
<li>Add ability to default to the "stable" branch of a module given in the
KDE Project database instead of always using the <code>master</code> branch. Feature
kindly contributed by Valery Yundin. This resurrects the <a href="/documentation/conf-options-table.html#conf-use-stable-kde">use-stable-kde</a>
option. (<a href="https://bugs.kde.org/show_bug.cgi?id=297357">Bug 297357</a>)</li>
<li>kdesrc-build now will prefer to name any git branches it must create after
the corresponding remote branch. kdesrc-build will not overwrite a local
branch name however (and it will even prefer an existing local branch if it's
pointing at the appropriate remote branch). (<a href="https://bugs.kde.org/show_bug.cgi?id=294347">Bug 294347</a>)</li>
<li>Metadata related to KDE Project support is no longer downloaded if no modules
to be built are "KDE Project" modules (although keep in mind <em>any</em> unknown
modules are now assumed to be of this category).</li>
<li>KDE Project metadata is now "mirrored", so it is only downloaded from the KDE
servers if it has changed. This can significantly reduce bandwidth usage for
repeated runs of kdesrc-build over a short time.</li>
<li>Add support for the <a href="http://bazaar.canonical.com/">Bazaar</a> source code
management tool, since the <code>libdbusmenu-qt</code> library that KDE depends on uses
it. (Note that your distro-provided packages of this library should normally
be sufficient, but if you wish to run the bleeding-edge version, now you can)
Patch provided by Aurélien Gâteau/Canonical.</li>
<li>The <code>perl</code> executable as provided in the <code>PATH</code> environment variable is now
used instead of hard-coding "<code>/usr/bin/perl</code>".</li>
<li>The <a href="/documentation/conf-options-table.html#conf-prefix">prefix</a> option now supports the <code>$MODULE</code> variable, similar to the
existing support in <a href="/documentation/conf-options-table.html#conf-module-base-path">module-base-path</a>. Patch provided by Raphael Kubo da
Costa.</li>
</ul>

<h3>Refactorings:</h3>

<p>For the most part these should have no visible change from the end-user
perspective. These are listed since they are the most likely cause of 
regressions or new bugs.</p>

<ul>
<li>Commands that read utility output (e.g. <code>git</code>, <code>svn</code>) use a common routine
to disable localization so that kdesrc-build can process the output.
(<a href="https://bugs.kde.org/show_bug.cgi?id=292143">Bug 292143</a>)</li>
<li>Renamed <code>ksb::Phases</code> to <code>ksb::PhaseList</code>.</li>
<li>A lot of refactoring to the update and build code. Subversion handling code
is properly modularized (git will follow with the next release).</li>
<li>Many utility methods moved to ksb::Util.</li>
<li>Perl internal exceptions should give a stack trace like kdesrc-build
exceptions.</li>
<li>kdesrc-build exceptions are handled in a single fashion now.</li>
<li>A module now has a concept of a separate "scm type" and "build type" instead
of just a "module type". E.g. you can have a Module that uses git for the scm
and qmake for the build system.</li>
<li>Some refactoring of the IPC system (used for async build/update) to move the
common code to the already-existing <code>BaseIPC</code> instead of having two root
IPC classes.</li>
<li>Occasional dead code removal.</li>
<li><code>svn</code> conflicted code check occurs following update instead of prior to
every build.</li>
<li>A "generic" build system is assumed until it's possible to read the source
code to allow the process to proceed in <a href="/documentation/supported-cmdline-params.html#cmdline-pretend">--pretend</a>-mode.</li>
<li>Use <code>Text::ParseWords</code> instead of custom quote-splitting.</li>
<li>Build system in use is shown with <a href="/documentation/supported-cmdline-params.html#cmdline-verbose">--verbose</a> and <a href="/documentation/supported-cmdline-params.html#cmdline-debug">--debug</a>.</li>
<li>The branch being checked out is normally shown.</li>
<li>If a module being built is part of a named module-set, that module-set is
shown (e.g. "Building juk from kdemm (1/1)")</li>
<li>Logged output includes the current directory now.</li>
<li>Improvements to various internal list utility functions.</li>
<li>Some <code>svn</code> command have their output examined. kdesrc-build now performs this
examination during <code>svn</code> execution instead of laboriously re-reading the
generated log files at the end.</li>
<li>package method export code has been refactored such that there is only one
export method (one of the two originals had a bug, although it was not
noticeable at the time).</li>
</ul>

<?php
  include("footer.inc");
?>
