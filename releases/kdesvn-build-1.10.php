<?php

  $release_version = '1.10';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2009-Oct-24</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?>

<?php if(file_exists("$release_file.asc")) {
  echo "<br>Code signature: <a href=\"$release_file.asc\">PGP signature</a> - key id 0x7B6AE9F2";
} ?>
</p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.9.1.php">1.9.1</a>):</p>

<h3>New features/changes:</h3>
<ul>
  <li>Add support for using <a href="http://git-scm.com/">Git source
  control</a>.  This is required by the <a
  href="http://techbase.kde.org/Getting_Started/Build/KDE4/Prerequisites#Qt">qt-copy</a>
  and <a
  href="http://techbase.kde.org/Getting_Started/Sources/KDE_git-tutorial">Amarok</a>
  modules.</li>

  <li>The included sample configuration file has comments for qt-copy and amarok describing
  what you need to do to build them.  Obviously if you don't already have git installed,
  you will need to do that first.  Afterwards you will need to add the correct
  <tt>repository</tt> option for each module (just uncomment the one you want from the
  sample file).  You may select the branch you want using the already-existing
  <tt>branch</tt> option.</li>

  <li>All support for building KDE 3 modules has been removed.</li>

  <li>A command line option, <tt>--purge-old-logs</tt> has been added to make kdesvn-build
  delete old and unused log directories after the build is finished (this feature was already
  introduced in <a href="kdesvn-build-1.9.php">kdesvn-build 1.9</a>).</li>

  <li>You may use <tt>--no-src</tt> and <tt>--src-only</tt> as aliases for the
  equivalent <tt>--no-svn</tt> and <tt>--svn-only</tt> options.</li>

  <li>kdesvn-build-generated log files now begin with a comment containing the command
  line of the command that generated that file.</li>

  <li>kdesvn-build performs build directory cleaning in a sub-process now, to avoid the
  useless "No log files" message that is generated if it fails to do this (usually due
  to permissions errors).</li>

  <li>This web site is no longer checked when downloading Subversion module snapshots,
  since the snapshots maintained here are out-of-date.</li>

  <li>kdesvn-build now works around a Subversion pecularity when the topleve source
  directory is a symlink.</li>
</ul>

<h3>Bugfixes:</h3>
<ul>
  <li>The <tt>svn info</tt> command is now run using a standard locale so that
  the output is as expected by kdesvn-build (KDE <a
  href="https://bugs.kde.org/show_bug.cgi?id=200302">bug 200302</a>).</li>

  <li>Do not crash if a config file is not found.  (Note, it remains a bad idea to not use
  a config file at all, as I do not test this mode of operation more than about once a
  year.  If you encounter a bug please let me know and I'll fix it though).</li>
</ul>

<?php
  include("footer.inc");
?>
