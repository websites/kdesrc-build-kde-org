<?php
  $release_version = '1.12.1';
  $release_title = "kdesrc-build $release_version";
  $release_file = "kdesrc-build-$release_version.tar.bz2";
  $page_title = "kdesrc-build release $release_version";
  $site_root = "../";

  include("header.inc");
?>

<p>Released: <b>2011-Jan-18</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?>

<?php if(file_exists("$release_file.asc")) {
  echo "<br>Code signature: <a href=\"$release_file.asc\">PGP signature</a> - key id 0x7B6AE9F2";
} ?>
</p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.12.php">1.12</a>):</p>
<!-- Excuse the HTML below, I got tired of writing it and simply used a
Markdown-to-HTML converter -->

<h3>New features/changes:</h3>
<ul>
<li><p>Perhaps the most significant change to aid the conversion to git modules is
support for easily adding git modules to your kdesrc-buildrc. It is done in
two parts:</p>

<ol><li><p>The <a href="http://kdesrc-build.kde.org/documentation/conf-options-table.html#conf-git-repository-base">git-repository-base</a> option, which sets
up a new repository alias. For instance:</p>

<p>global
    git-repository-base kde-git git://git.kde.org
end global</p></li></ol>

<p>would setup the <code>kde-git</code> alias for git://git.kde.org (saving you from always
having to type out git://git.kde.org/). You would then use this with the new
<a href="http://kdesrc-build.kde.org/documentation/kde-modules-and-selection.html#module-sets">module-set</a> feature in association with:</p>

<ol><li><p>The <a href="http://kdesrc-build.kde.org/documentation/conf-options-table.html#conf-use-modules">use-modules</a> option, such as in this example:</p>

<p>... <br />
end global</p>

<p>...</p>

<p>module-set <br />
    repository kde-git # Note, no URL is spelled out <br />
    use-modules automoc cagibi attica soprano polkit-qt-1 phonon <br />
end module-set</p></li></ol>

<p>This <code>module-set</code> declares a new module for everything listed in <code>use-modules</code>,
basing the module's repository option on the <code>repository</code> given for the module set.
Other options can be specified in the module set, and they will be passed on to
each individual module declared. See the sample rc file or the documentation
for more details and examples.</p></li>
<li>kdesrc-build will optionally <em>not</em> build a module if there were no changes to
the source directory after the update phase is complete. This is sometimes
incorrect, but can be a moderate speedup for infrequently-changed modules.
See the <a href="http://kdesrc-build.kde.org/documentation/conf-options-table.html#conf-build-when-unchanged">build-when-unchanged</a> option. In addition,
you can use the <a href="http://kdesrc-build.kde.org/documentation/supported-cmdline-params.html#cmdline-no-build-when-unchanged">--force-build</a> option to force the
build phase to run for all modules even if you've enabled this option.</li>
<li>kdesrc-build will now display the number of failing tests when using the
<a href="http://kdesrc-build.kde.org/documentation/conf-options-table.html#conf-run-tests">run-tests</a> option.</li>
<li>You can set the <a href="http://kdesrc-build.kde.org/documentation/conf-options-table.html#conf-run-tests">run-tests</a> option to <code>upload</code>, in which case
<code>make Experimental</code> is run instead of <code>make test</code>, and the results of the test
suite are uploaded to <a href="http://my.cdash.org/">CMake's CDash</a></li>
<li>kdesrc-build has changed the way it updates git modules slightly. It should
now support changing the <a href="http://kdesrc-build.kde.org/documentation/conf-options-table.html#conf-repository">repository</a> option. In addition,
it now uses <code>git pull --rebase</code> to merge in KDE upstream changes. If you are
a developer this should make it easier to avoid needless merge commits. If
you are not a developer then things should work the same as before.</li>
<li>Debugging output is now also colorized by default (if not outputting to a
file).</li>
</ul>

<h3>Bugfixes:</h3>
<ul>
<li>Updates to kdesrc-buildrc-sample to account for updates to the KDE software
repository layout. <strong>Note:</strong> The KDE Project is currently migrating from
the existing Subversion repository to use a <a href="http://projects.kde.org/">Git repository</a>.
Because of this, there may be updates required to your kdesrc-buildrc, even
after this release. The next version of kdesrc-build will support building
modules dynamically, as the software repository changes (or manually, as
before). (<a href="https://bugs.kde.org/show_bug.cgi?id=245248">Bug 245248</a>)</li>
<li>The default for the <a href="http://kdesrc-build.kde.org/documentation/conf-options-table.html#conf-binpath">binpath</a> option is now empty. The $PATH
environment variable is still used as before when building KDE, so this
change should not affect you, but should result in a cleaner $PATH when
building.</li>
<li>kdesrc-build now hides git module cloning messages again, with the exception
of Nokia's Qt software checked out from gitorious.org. (The gitorious.org git
software contains a bug which is mitigated by showing git checkout output --
if you are unable to checkout qt-copy, make sure you're checking it out from
a terminal without redirecting the output of kdesrc-build) (<a href="https://bugs.kde.org/show_bug.cgi?id=262145">Bug
262145</a>)</li>
<li>A stray "kdesvn/" path default has been replaced with "kdesrc/" for consistency
with the rest of the script.</li>
<li>kdesrc-build will ensure that KDE libraries and programs are preferred to Qt
ones when building KDE, even if the system Qt install is being used.</li>
<li>kdesrc-build properly checks whether the "ssh-agent" is required for Git
modules hosted at git.kde.org.</li>
<li>kdesrc-build now shows the number of commits that were applied to git-based
modules between the current update and the last update (similar to how
Subversion-based modules show the number of files affected). kdesrc-build
also shows the number of files updated when performing the initial clone of a
git-based module.</li>
<li>Fixed building of l10n modules (used by the <a href="http://kdesrc-build.kde.org/documentation/conf-options-table.html#conf-kde-languages">kde-languages</a>
option. (<a href="https://bugs.kde.org/show_bug.cgi?id=262364">Bug 262364</a>)</li>
<li>kdesrc-build now waits for l10n modules to complete their source update
before attempting to build. (<a href="https://bugs.kde.org/show_bug.cgi?id=262369">Bug 262369</a>)</li>
<li>The <code>-qt-git</code> configure flag in the kdesrc-buildrc-sample has been removed, as
it is no longer used by Qt (Qt always includes GIF support now). If you have
this flag in your qt-copy <code>configure-flags</code>, remove it. (<a href="https://bugs.kde.org/show_bug.cgi?id=262076">Bug 262076</a>)</li>
<li>git modules now show an appropriate "Updating..." message if <a href="http://kdesrc-build.kde.org/documentation/supported-cmdline-params.html#cmdline-src-only">--src-only</a>
is used.</li>
</ul>

<?php
  include("footer.inc");
?>
