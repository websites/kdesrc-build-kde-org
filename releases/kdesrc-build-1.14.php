<?php
  $release_version = '1.14';
  $release_title = "kdesrc-build $release_version";
  $release_file = "kdesrc-build-$release_version.tar.bz2";
  $page_title = "kdesrc-build release $release_version";
  $site_root = "../";

  include("header.inc");
?>

<p>Released: <b>2011-Sep-17</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?>

<?php if(file_exists("$release_file.asc")) {
  echo "<br>Code signature: <a href=\"$release_file.asc\">PGP signature</a> - key id 0x7B6AE9F2";
} ?>
</p>

<p><?php echo $release_title; ?> had 185 commits from 8 authors from the prior
release (<a href="kdesrc-build-1.13.php">1.13</a>). The changes are summarized
below:</p>

<h3>New features/changes:</h3>
<ul>

<li><p>References to qt-kde in the qt-copy module should be replaced by
<tt>qt</tt>. The 'qt-kde' module no longer exists in the KDE project database.
This change has already been made to the qt-copy module in the sample
configuration file.
</p></li>

<li><p>There is a <b>new on-disk layout for source and build directories</b>
for modules that come from the KDE project database (i.e. that use
<tt>repository kde-projects</tt>). To avoid needless re-cloning and
re-building, kdesrc-build will automatically move old source and build
directories for affected modules the first time they are updated or built.
</p>

<p>The new layout is designed to reflect the logical database layout. As an
example, kdesrc-build would be checked out to
<em>source-dir</em>/extragear/utils/kdesrc-build to reflect its layout in the
project database, while kdelibs would be checked out to
<em>source-dir</em>/kde/kdelibs.
</p></li>

<li><p>Code removal/consolidation by using Perl core modules where feasible.
The minimum Perl version required is 5.10.1.
</p></li>

<li><p>A manpage for kdesrc-build was added. If you install kdesrc-build
yourself don't forget to add the KDE man pages directory to your MANPATH, or
tell CMake to install your KDE man pages to a directory in your MANPATH.
</p></li>

<li><p>A simplistic script called <tt>kdesrc-build-setup</tt> has been added
to generate a (very) simple configuration file for you. The resultant
configuration file is not nearly as commented as the sample configuration
file, but it will at least get you started if you have no existing
configuration file. The script can (and in fact must) be run from the command
line so that you can use it to configure kdesrc-build as long as you can run
kdesrc-build itself.
</p></li>

<li><p>Git supports user-defined URI schemes to find repositories over the
network. The KDE system administrators have a <a
href="http://community.kde.org/Sysadmin/GitKdeOrgManual">recommended</a>
'kde:' URI scheme, which kdesrc-build will automatically define for you if you
do not already have it set. The sample configuration file uses the 'kde:' URI
for modules that do not come from the KDE project database.
</p></li>

<li><p>There is a syntax (<em>module-name/</em>*) which you can use in a
<tt>use-modules</tt> option to build all modules <em>descending</em> from a
given module, without including the parent. This might be useful if the parent
module has a source code repository of its own only to build its child modules
(which kdesrc-build will already do, so the parent module is not needed).
Normally such parent modules are marked inactive in the project database,
without this change kdesrc-build will ignore that module (and its children!).
This syntax is only supported for the <tt>kde-projects</tt> repository.
</p>

<p>As an example:</p>
<pre>module-set kdegraphics
  repository kde-projects
  # We cannot build "kdegraphics" directly, but we can build its children.
  use-modules kdegraphics/libs kdegraphics/*
end module-set</pre></li>

<li><p>The <tt>--purge-old-logs</tt> command line option has been removed (it
can still be altered as a <a
href="/documentation/supported-cmdline-params.html#cmdline-global-option">generic
option</a>). Instead it has been made the default action for kdesrc-build --
in other words kdesrc-build will start removing old log directories by
default.
</p></li>

<li><p>kdesrc-build supports using Git snapshot tarballs for Git modules from
the KDE project database, for performing the initial module clone. This is
done by default, and helps reduce load on the KDE project infrastructure.
</p></li>

<li><p>A new keyword is supported in the configuration file, <tt>include</tt>.
This can be used to include other files into the configuration file, allowing
for having a file holding common options to be included into different project
kdesrc-buildrc files.
</p></li>

<li><p>This is more of a change for packagers, but the kdesrc-build
distribution tarball now comes completely from git. Packagers should be able to
run <tt>git archive --prefix=kdesrc-build-1.14/ v1.14 | bzip2 &gt;
kdesrc-build-1.14.tar.bz2</tt> to get the same tarball as included in the
download link above.
</p></li>

<li><p>A command line option, <tt>--no-tests</tt> was added, which works
substantially the same as the existing <tt>--no-build</tt>, <tt>--no-src</tt>,
etc.
</p></li>

<li><p>The output of the <tt>--help</tt> option has been revised and
significantly reduced in length.
</p></li>

<li><p>The DocBook documentation has been improved slightly as well.
</p></li>

<li><p>A large internal refactoring has been performed to make the code
structure more modular. The long-term goal is to ensure kdesrc-build continues
to be maintainable and to allow for quick feature additions. There should be
no user-visible effect (except that kdesrc-build might tell you what module
set a given module is being built from since that information is now tracked
for longer internally)
</p></li>

<li><p>Some "Would have created directory" messages are suppressed in pretend mode
after their first time displayed.
</p></li>

<li><p>The sample configuration uses KDE Project database modules exclusively.
</p></li>

<li><p>A warning message is displayed if a requested KDE project database
module does not contain any repositories that can be built.
</p></li>

<li><p>kdesrc-build will now alter the default <tt>origin</tt> Git remote
instead of its previous alternative remote identifier. If you have been
tracking this manually then either do not use kdesrc-build for that source
directory, or ensure you keep your ~/.kdesrc-buildrc file in sync with your
desired repository.
</p></li>

<li><p>Subversion tarball snapshot support removed since most KDE modules are
now in Git.
</p></li>

<li><p>Post-build error email support removed.
</p></li>

</ul>

<h3>Bugfixes:</h3>
<ul>
<li><p>+module syntax actually works. (This syntax on the command line forces
kdesrc-build to assume that the given module is from the KDE project database
even if you haven't defined it in your ~/.kdesrc-buildrc).
</p></li>

<li><p>Various fixes/updates to the sample configuration file.
</p></li>

<li><p>Also check environment variables queued to be set when trying to
determine the proper environment variable setting for environment variables
that handle a list of directory paths (e.g. PATH, LD_LIBRARY_PATH).
</p></li>

<li><p>The KDE Project database is downloaded in pretend mode when it's needed
(as has always been indicated on the kdesrc-build output). In addition only 1
download per run is attempted.
</p></li>

<li><p>The exception handler makes a more proper check for a Perl "object"
before trying to make a method call. Realistically this just changes the error
message you eventually would get.
</p></li>

<li><p>"C" locale is properly set when running commands that kdesrc-build must
analyze the output of.
</p></li>

<?php
  include("footer.inc");
?>
