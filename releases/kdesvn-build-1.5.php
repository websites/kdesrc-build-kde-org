<?php

  $release_version = '1.5';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2007-Oct-22</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?></p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.4.1.php">1.4.1</a>):</p>

<h3>Bugfixes</h3>
<ul>
<li>kdesvn-build now downloads unsermake from the correct location (it was moved between
   this release and the last).</li>
<li>Config file reading is more tolerant of leading whitespace. (Bug <a
   href="http://bugs.kde.org/show_bug.cgi?id=146127">146127</a>)</li>
<li>Fixed the --nice command line option to recognize a value of 0 (which
   disables renicing).</li>
<li>kdesvn-build works with the changed l10n module layout in KDE SVN.  This
   makes the kde-languages option work again. (Bug <a
   href="http://bugs.kde.org/show_bug.cgi?id=150738">150738</a>)</li>
<li>kdesvn-build will now warn if the same module is defined twice in the config
   file.  (Bug <a href="http://bugs.kde.org/show_bug.cgi?id=150676">150676</a>)</li>
</ul>

<h3>New features</h3>
<ul>
<li>The extragear and playground modules can now be built in their entirety in
   one shot.
   i.e. having
   <blockquote><font face="monospace">
   module extragear<br>
   end module
   </font></blockquote>
   in the config file now works.</li>
</ul>

<?php
  include("footer.inc");
?>
