<?php

  $release_version = '0.97.5';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p><?php echo $release_title; ?> is on the <b>KDE 3.5</b> development track.  If you intend to build KDE 4 with this release, it may still be possible, but is much more trouble than it's worth.  (<em>Note that it is still possible to build earlier KDE releases using the appropriate branch/tag options</em>).  Users intending to build KDE 4 should use
<a href="kdesvn-build-<?php echo $devel_version; ?>.php">kdesvn-build <?php echo $devel_version; ?></a>
</p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?></p>

<p><?php echo $release_title; ?> had the following changes from the prior release:</p>

<ul>

<li>Fix bug where kdesvn-build did not support the line continuation character (\) when loading its configuration file.  The sample configuration file has been adjusted as well.</li>

<li>Fix <a href="http://bugs.kde.org/show_bug.cgi?id=111167">bug 111167</a>, now kdesvn-build will warn you if the directory a module was checked out from is different from what it should be (which can happen if you change the branch or tag option without re-checking out).</li>

<li>The /admin directory symlink is now re-created whenever a module is built or installed, previously it was only re-created when running make -f Makefile.cvs, which caused install errors on some systems.</li>

<li>Fix a regression where setting empty options no longer worked.  You can now set an option to an empty value again.</li>

<li>-march=i686 setting removed from default configuration as it interferes with non-32-bit x86 systems.  It is still included in the sample configuration file for you to configure.</li>

</ul>

<?php
  include("footer.inc");
?>