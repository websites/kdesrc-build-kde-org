<?php
  $release_version = '1.15.1';
  $release_title = "kdesrc-build $release_version";
  $release_file = "kdesrc-build-$release_version.tar.xz";
  $page_title = "kdesrc-build release $release_version";
  $site_root = "../";

  include("header.inc");
?>

<p>Released: <b>2012-May-21</b></p>

<p>Download it:
<a href="http://download.kde.org/stable/kdesrc-build/1.15.1/src/kdesrc-build-1.15.1.tar.xz"><?php echo $release_file; ?></a>
-
<a href="http://download.kde.org/stable/kdesrc-build/1.15.1/src/kdesrc-build-1.15.1.tar.xz.mirrorlist">Details (incl. PGP signature)</a>
</p>

<p><?php echo $release_title; ?> had 6 commits from 1 author from the prior
release (<a href="kdesrc-build-1.15.php">1.15</a>). The changes are summarized
below:</p>

<h3>Bugfixes:</h3>
<ul>
<li>The test suite is run from the temporary directory setup for the test
instead of the current directory. (<a href="https://bugs.kde.org/show_bug.cgi?id=299579">Bug 299579</a>)</li>
<li>Restore the remove-after-install feature (it could not have been working
due to a long-ago refactoring.</li>
<li>Search for qmake under more names (those given in the similar CMake
checks). (<a href="https://bugs.kde.org/show_bug.cgi?id=299577">Bug 299577</a>)</li>
<li>Fix an uninitialized variable warning when using --debug.</li>
</ul>

<?php
  include("footer.inc");
?>
