<?php

  $release_version = '1.4.1';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2007-May-11</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?></p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.4.php">1.4</a>):</p>

<h3>Bugfixes</h3>
<ul>
<li>Added kdepimlibs module in the correct position in the sample configuration
file.  This module is required to build KDE 4, and must be built after kdelibs
but before kdebase.  It does not exist, however, in KDE 3.</li>
<li>Fixed a bug where kdesvn-build would still try to download a trunk
snapshot even when the tag option was specified for a module.</li>
</ul>

<?php
  include("footer.inc");
?>
