<?php

  $release_version = '1.1';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2006-Apr-04</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?></p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.0.php">1.0</a>):</p>

<h3>Bugfixes</h3>
<ul>
<li>use-qt-builddir-hack is no longer optional.  kdesvn-build will always make
  qt-copy into a separate build directory, even for Qt 3.</li>
<li>Re-enable apply-qt-patches for qt-copy 4.</li>
<li>qt-copy 4 is correctly installed now.</li>
<li>The qt-copy apply_patches script is run in the source directory rather than the
  build directory.</li>
<li>Slightly improve the check to see if unsermake is required, to only search against
  modules that we are building and that the user wants unsermake for.</li>
<li>Use KDEDIRS rather than KDEDIR, which has been deprecated since the KDE 2 days.</li>
<li>Do not ignore configure flags for the l10n module. (bug <a href="http://bugs.kde.org/show_bug.cgi?id=116621">116621</a>).</li>
</ul>

<h3>Feature Additions</h3>
<ul>
<li><p>
kdesvn-build supports the <a href="http://www.cmake.org/">CMake build system</a>, which is required to build
  KDE 4.  kdelibs /trunk and a few other modules already require CMake.
  kdesvn-build will automatically do a clean rebuild if you were using
  unsermake before.  See the <a href="http://wiki.kde.org/tiki-index.php?page=KDECMakeIntro">KDE 4
  with CMake Wiki</a> for more information.
</p>
<p>The new "cmake-options" option replaces "configure-flags" for modules that use
  CMake.  See the CMake section in the documentation for more details (available
  online at <a href="/documentation/">the documentation section</a>)  Note that CMake has a
  different command line syntax than configure.
</p>
</li>
<li>Add --run option to run a given command with the same environment that
  kdesvn-build normally runs programs in.  This isn't really helpful for
  shells since they generally reset the environment but it may be useful
  for developers.
</li>
<li>kdesvn-build handles the -prefix option for the Qt 4 configure automatically.</li>
</ul>

<?php
  include("footer.inc");
?>
