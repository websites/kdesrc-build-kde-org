<?php
  $page_title = "kdesvn-build release 0.97.3";
  $site_root = "../";
  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>kdesvn-build 0.97.3 is on the <b>KDE 3.5</b> development track.  If you intend to build KDE 4 with this release, it may still be possible, but is much more trouble than it's worth.  (<em>Note that it is still possible to build earlier KDE releases using the appropriate branch/tag options</em>).
</p>

<p>Download it: <a href="kdesvn-build-0.97.3.tar.bz2">kdesvn-build-0.97.3.tar.bz2</a>
<?php echo niceFileSize("kdesvn-build-0.97.3.tar.bz2"); ?></p>

<p>kdesvn-build 0.97.3 had the following changes from the prior release:</p>

<ul>

<li>Fixed bug with qt-copy, where kdesvn-build would default back to downloading Qt 4 if you mentioned qt-copy in the configuration file.   This also affected the sample configuration file.</li>

<li>kdesvn-build will normally display when the build directory is being cleaned using rm -rf, but didn't show a message when it was done.  This had the unfortunate effect of making the rm -rf look like it was erasing more than it should be.  This has been fixed.</li>

</ul>

<?php
  include("footer.inc");
?>