<?php

  $release_version = '1.9';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2009-May-29</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?>

<?php if(file_exists("$release_file.asc")) {
  echo "<br>Code signature: <a href=\"$release_file.asc\">PGP signature</a> - key id 0x7B6AE9F2";
} ?>
</p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.8.php">1.8</a>):</p>

<p><b>Note:</b> KDE 3.5-series build support has not been tested in some time, but should still work.
This is <u>the last kdesvn-build release</u> where that will be true, I plan on removing all KDE 3-specific
code before the next release.</p>

<h3>New Features:</h3>
<ul>
  <li><p>kdesvn-build can now run unit tests for a KDE module, if present (best example is kdelibs).  This is
    controlled by the new <code>run-tests</code> option.  Example output:
    <pre>        [...]
        Build succeeded after 14 minutes, and 48 seconds.
        All tests ran successfully.
        Installing kdelibs.</pre></p>
    <p>See <a href="https://bugs.kde.org/show_bug.cgi?id=167334">KDE bug 167334</a>.</p>
  </li>

  <li><p>kdesvn-build can now remove old unused log directories automatically.  This can potentially save a lot
    of disk space if you have used kdesvn-build for a long time.  This is controlled by the new
    <code>purge-old-logs</code> option.</p>

    <p>All unused log directories are removed at the end of the build if this option is in effect.</p>

    <p>If you want to perform a one-time removal, you can use a command like this: <code>kdesvn-build
    --no-svn --no-build --no-install --purge-old-logs=true kdelibs</code></p>
</ul>

<h3>Bugfixes:</h3>
<ul>
  <li>kdesvn-build now sets the <a href="http://www.kdedevelopers.org/node/3800">CMAKE_PREFIX_PATH</a>
    variable when building KDE modules.  This essentially makes CMake prefer software packages installed
    from kdesvn-build over packages already present on the system if both are present.  This fixes build
    errors from using incompatible software.</li>
  <li>Fix a bug where kdesvn-build would run <code>svn</code> without forcing the output to be English
    even though English output was expected.</li>
  <li>Do not warn about consecutive build failures for modules that kdesvn-build isn't working on at
    the time.</li>
  <li>kdesvn-build now skips the conflicted source code check if a source code update is not performed,
    since kdesvn-build would have performed the check last time the source was updated.</li>
  <li>Fix a problem in environment handling on some systems, which would result in odd
    error messages and warnings.</li>
</ul>

<?php
  include("footer.inc");
?>
