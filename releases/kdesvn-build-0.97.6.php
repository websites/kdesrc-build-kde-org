<?php

  $release_version = '0.97.6';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p><?php echo $release_title; ?> is on the <b>KDE 3.5</b> development track.  If you intend to build KDE 4 with this release, it may still be possible, but is much more trouble than it's worth.  (<em>Note that it is still possible to build earlier KDE releases using the appropriate branch/tag options</em>).  Users intending to build KDE 4 should use
<a href="kdesvn-build-<?php echo $devel_version; ?>.php">kdesvn-build <?php echo $devel_version; ?></a>
</p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?></p>

<p><?php echo $release_title; ?> had the following changes from the prior release:</p>

<ul>

<li>kdesvn-build will now show the correct path to the rc-file (if it needed to
be displayed) when the rc-file was not the default location.</li>

<li>kdesvn-build doesn't try to grab its exclusion lock when you are only using
it in pretend mode now.</li>

</ul>

<?php
  include("footer.inc");
?>