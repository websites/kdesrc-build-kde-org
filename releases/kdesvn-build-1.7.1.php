<?php

  $release_version = '1.7.1';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2008-Oct-06</b></p>
<p><em>Codename: Tacoma Narrows</em></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?>

<?php if(file_exists("$release_file.asc")) {
  echo "<br>Code signature: <a href=\"$release_file.asc\">PGP signature</a> - key id 0x7B6AE9F2";
} ?>
</p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.7.php">1.7</a>):</p>

<h3>Bugfixes:</h3>
<ul>
  <li>Correctly create the qt-copy configure script.  (Broken after 1.7's change to automagically
  reconfigure when necessary).</li>
  <li>Don't add weird configure flags when no configure-flags for qt-copy were specified.</li>
  <li>Fix up some "uninitialized value" warnings to allow configuration to proceed when the
  persistent data store introduced in 1.7 is not present.</li>
</ul>

<p>Also see <a href="http://bugs.kde.org/show_bug.cgi?id=172288">KDE bug 172288</a>.</p>

<?php
  include("footer.inc");
?>
