<?php

  $release_version = '0.98.2';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p><?php echo $release_title; ?> is on the <b>KDE 4.0</b> development track.  If you intend to build KDE 3 with this release, it is possible, but it would be much easier to use <a href="kdesvn-build-<?php echo $current_version; ?>.php">kdesvn-build <?php echo $current_version;?></a> instead.
</p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?></p>

<p><?php echo $release_title; ?> had the following changes from the prior release:</p>

<h3>Bugfixes</h3>
<ul>

<li>If kdesvn-build needs to display the path to the configuration file, it will now always
display the actual path that it used instead of the default path.</li>

<li>The mutual exclusion lock is no longer taken when running in pretend mode.  This means
that you can run kdesvn-build in pretend mode even while kdesvn-build is already running.
</li>

</ul>

<h3>Feature Additions</h3>
<ul>

<li>More options can be set on a per module basis, which used to be only possible to set
globally.  These options are:

  <ul>
    <li><tt>source-dir</tt> (which chooses the directory to download the
    sources into)</li>

    <li><tt>svn-server</tt> (which controls the Subversion server to download
    the sources from)</li>

    <li><tt>qtdir</tt> (which controls the location of the Qt library)</li>

    <li><tt>kdedir</tt> (which controls where KDE is installed to)</li>
    
    <li><tt>binpath</tt> (controls the setting of the PATH environment variable
    while kdesvn-build is running)</li>

    <li><tt>libpath</tt> (controls what libraries are added to LD_LIBRARY_PATH
    while kdesvn-build is running)</li>
  </ul>

</li>

</ul>

<?php
  include("footer.inc");
?>
