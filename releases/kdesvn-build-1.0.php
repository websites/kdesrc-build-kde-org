<?php

  $release_version = '1.0';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2006-Feb-02</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?></p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-0.98.2.php">0.98.2</a>):</p>

<h3>Bugfixes</h3>
<ul>
<li>Removed some instances where an undefined variable was used, leading to Perl warnings.</li>
<li>Removed a rather useless feature where kdesvn-build would try to guess what error caused a module
to fail to build.  The guess was wrong most of the time.</li>
<li>Simplified qt-copy configure flags.</li>
<li>Removed some unused options, including --resume.  --resume-from still works and is supported.</li>
<li>Fixed a bug where having multiple module/subdirectory references on the command line were interpreted
incorrectly.  For example, <tt>kdesvn-build --svn-only kdelibs/khtml kdebase/konqueror</tt> would not work
right.</li>
</ul>

<h3>Feature Additions</h3>
<ul>

<li>kdesvn-build supports loading Subversion snapshots of a module checkout from this website, automatically
and with no user setup required.  When <a href="/other/svn-checkout-tarballs.php">the Subversion snapshot</a> is
available from this site, it can lead to a significant savings in time and bandwidth for the initial checkout of
a module, helping both you and the kde.org Subversion mirrors.</li>

<li>kdesvn-build sports improved support for KDE 3.5.  KDE 3.5 users were initially encouraged to use the 0.97.x
release series of kdesvn-build, although 0.98 supported 3.5.  But now, you can use the "use-stable-kde" option,
which is described in <a href="/documentation/kdesvn-buildrc.html#conf-use-stable-kde">the documenation</a> and in
the included kdesvn-buildrc-sample file.  Setting this option to true will cause kdesvn-build to download modules
from their KDE 3.5 branches whenever possible by default, instead of downloading KDE 4.0.
</li>

<li><p>Added a feature to automatically download, build, and install a language translation package from KDE's
advanced translation module, l10n.  You simply add the <a href="/documentation/kdesvn-buildrc.html#conf-kde-languages">kde-languages</a> option to your ~/.kdesvn-buildrc,
with the name codes of the language you wish to use.  (The list of two-letter name codes is available from the KDE
translation project at <a href="http://i18n.kde.org/teams/">http://i18n.kde.org/teams/</a>.)  For instance, to
add the German translation, you would have the following line:</p>

<code>kde-languages de <i># German</i></code>

<p>Thanks go to levipenumbra for the initial idea and implementation for this feature.</p>
</li>

<li>kdesvn-build now tries much harder to avoid being frozen by subcommands which are expecting user input.
Users who are upgrading kdesvn-build should consult the kdesvn-buildrc-sample file's <b>make-install-prefix</b>
option to see what flags to pass to sudo.  Also, kdesvn-build will automatically accept the self-signed SSL
signature for svn.kde.org to avoid having Subversion prompt you to do so manually.</li>
</ul>

<h3>Minor feature additions:</h3>

<ul>
<li>The toplevel build directory is preserved while deleting its contents now.  This means that you can have
a symlink for your build directory pointing somewhere else, for instance.
</li>

<li>The default CXXFLAGS and configure flags have been changed based on advice from some of KDE's compiler
experts.</li>

<li>Also, the -march= option has been removed from all defaults, as there is no good default for the option.  (This
option selects what CPU to optimize for).</li>

<li>unsermake is unable to build <a href="/documentation/developer-features.html#building-apidox">API documentation</a> for modules, so now kdesvn-build will refuse to build API documentation with unsermake.
unsermake is still the default build system.  Developers should consider using the 'kde:ClassName' shortcut
or going to <a href="http://www.englishbreakfastnetwork.org/apidocs/">English Breakfast Network</a> to consult
online API documentation for KDE.</li>

</ul>

<?php
  include("footer.inc");
?>
