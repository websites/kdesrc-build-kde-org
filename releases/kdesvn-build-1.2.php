<?php

  $release_version = '1.2';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2006-May-05</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?></p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.1.php">1.1</a>):</p>

<h3>Bugfixes</h3>
<ul>
<li>Fix handling of an empty make-options option with CMake.</li>
<li>Don't require the /admin directory for modules using CMake.</li>
<li>kdesvn-build no longer tries to download a Subversion snapshot if
    override-url is in effect for a module.</li>
<li>kdesvn-build doesn't override qt-copy options set from the command line now.</li>
<li>Don't warn if ssh-agent is not running on Mac OS X.</li>
<li>The branch option works correctly with playground/ and extragear/.</li>
<li>kdesvn-build passes arguments directory to the underlying program with no intermediate
   shell quoting.</li>
<li>kdesvn-build uses relative symlinks instead of absolute symlinks (meaning you can move
   log directories and the symlinks will still work).</li>
<li>Don't try to build arts or kdesupport by default if building KDE 4 (kdesupport is KDE 3,
   arts is muy deprecated).</li>
<li>kdesvn-build handles the branch option correctly with modules of the form KDE/foo now.</li>
</ul>

<h3>Feature Additions</h3>
<ul>
<li><p>kdesvn-build understands the special branch name <i>kde4-snapshot</i> when building
    kdelibs. i.e.:</p>
<pre>module kdelibs
  branch <b>kde4-snapshot</b>
end module</pre>

<p>will build the branches/work/kdelibs4_snapshot of kdelibs, required for kdebase, etc.</p></li>
<li>Make kde4-snapshot the default kdelibs branch, unless use-stable-kde is set to true.</li>
<li>kdesvn-build accepts the "trunk" branch name and treats it correctly.</li>
<li>When building CMake modules, don't try building the module 3 times (which worked sometimes
   with automake to get a successful build).</li>
<li>Simplified handling internally of the branch handling.  This also fixed a few minor bugs
   which apparently were never encountered by users.</li>
<li>Less useless output in pretend mode.</li>
<li>Intelligent handling of KDEDIRS.  If kdesvn-build is building a module that has a
   different "kdedir" setting than kdelibs, the kdelibs "kdedir" setting would be added to
   the end of kdebase's when building kdebase.</li>
<li>binpath now defaults to the $PATH setting from the environment.  You will want to ensure
   binpath is set correctly if using kdesvn-build from cron.</li>
</ul>

<?php
  include("footer.inc");
?>
