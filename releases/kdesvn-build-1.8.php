<?php

  $release_version = '1.8';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2009-Feb-22</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?>

<?php if(file_exists("$release_file.asc")) {
  echo "<br>Code signature: <a href=\"$release_file.asc\">PGP signature</a> - key id 0x7B6AE9F2";
} ?>
</p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.7.1.php">1.7.1</a>):</p>

<h3>Bugfixes:</h3>
<ul>
  <li>Allow modules to build even when more than one cmake-option is specified. (KDE
      <a href="http://bugs.kde.org/show_bug.cgi?id=172288">bug 172719</a>)</li>
  <li><p>The handling of the tag, branch, and module-base-path options required alteration due to
      changes over time in the layout of the KDE Subversion repository.
      Now non-KDE modules (notably qt-copy, kdesupport, and phonon) no longer add their own
      module name to the generated Subversion URL automatically.  This is because many tags/branches
      don't end in their module name anymore (as used to be the case).</p>
      <p>This change allows more tags/branches to be used.  However, existing tags, branches, and
      module-base-path options need to have <tt>/modulename</tt> added to the existing value.</p>
      <p>For example, <tt>tag kdesupport-for-4.2</tt> in the kdesupport module would become
      <tt>tag kdesupport-for-4.2/kdesupport</tt>.</li>
  <li>qt-copy's cxxflags no longer includes global cxxflags anymore.  This matches handling of
      configure-flags and allows for setting KDE-specific cxxflags globally.</li>
  <li>KDE now generates tarball snapshots for extragear/ and playground/ modules in addition to
      the rest of KDE sources, so now kdesvn-build will attempt to download snapshots for extragear/
      and playground/ as well.</li>
</ul>

<p>In addition a small test suite is available for this release, kdesvn-build 1.8 passes 20/20 tests.  The suite
is still very incomplete given the breadth of kdesvn-build but feedback is encouraged.</p>

<?php
  include("footer.inc");
?>
