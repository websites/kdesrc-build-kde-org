<?php
  $page_title = "kdesvn-build release 0.97.4";
  $site_root = "../";
  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>kdesvn-build 0.97.4 is on the <b>KDE 3.5</b> development track.  If you intend to build KDE 4 with this release, it may still be possible, but is much more trouble than it's worth.  (<em>Note that it is still possible to build earlier KDE releases using the appropriate branch/tag options</em>).
</p>

<p>Download it: <a href="kdesvn-build-0.97.4.tar.bz2">kdesvn-build-0.97.4.tar.bz2</a>
<?php echo niceFileSize("kdesvn-build-0.97.4.tar.bz2"); ?></p>

<p>kdesvn-build 0.97.4 had the following changes from the prior release:</p>

<ul>

<li>kdesvn-build now refers to <a href="http://kdesvn-build.kde.org/">kdesvn-build.kde.org</a> as the homepage.</li>

<li>kdesvn-build will now run <b>svn cleanup</b> automatically on an svn failure and then try again in order to complete successfully more often.</li>

<li>kdesvn-build now uses its own code to link the source directory to the build directory for modules that don't support it natively (e.g. qt-copy, kdebindings).  It used the lndir tool before, which apparently isn't completely portable.</li>

</ul>

<?php
  include("footer.inc");
?>