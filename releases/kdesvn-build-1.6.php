<?php

  $release_version = '1.6';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2008-Feb-13</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?></p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.5.php">1.5</a>):</p>

<h3>New features</h3>
<ul>
<li>kdesvn-build will now simultaneously build updated modules while downloading
updates for modules, which should improve the build time (sometimes significantly).

If you experience error messages which seem out of order it may be due to this.

This can be disabled using the --no-async option.</li>
</ul>

<?php
  include("footer.inc");
?>
