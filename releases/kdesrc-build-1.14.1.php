<?php
  $release_version = '1.14.1';
  $release_title = "kdesrc-build $release_version";
  $release_file = "kdesrc-build-$release_version.tar.bz2";
  $page_title = "kdesrc-build release $release_version";
  $site_root = "../";

  include("header.inc");
?>

<p>Released: <b>2011-Sep-21</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?>

<?php if(file_exists("$release_file.asc")) {
  echo "<br>Code signature: <a href=\"$release_file.asc\">PGP signature</a> - key id 0x7B6AE9F2";
} ?>
</p>

<p><?php echo $release_title; ?> had 12 commits from 2 authors from the prior
release (<a href="kdesrc-build-1.14.php">1.14</a>). The changes are summarized
below:</p>

<h3>New features/changes:</h3>
<ul>

<li><p>kdesrc-build will optionally remove directories that are preventing
a git-clone from succeeding on initial module download.

</p><p>The use case for this is that if you have an existing checkout of
kde-baseapps, the way that 1.14 and later releases of kdesrc-build decide to
layout the source code means that the kate and konsole applications will
nest under kde-baseapps.

</p><p>This is not inherently an issue, but older kde-baseapps checkouts may
already have a kate or konsole directory, which will cause kate or konsole to
fail to checkout and lead to confusing git error messages.

</p><p>If you pass the <b>--delete-my-patches</b> option kdesrc-build will
remove any directory that is in the way of a git-clone. Using this option
<em>might be dangerous</em> if you are a developer: kdesrc-build will tell you
when this option might apply, don't use it otherwise.
</p></li>

<li><p>Copyright/license information was made even more specific in the source
included with the distribution.
</p></li>

</ul>

<h3>Bugfixes:</h3>
<ul>

<li><p>The git snapshot support introduced in 1.14 downloads to $TMPDIR instead
of whatever directory kdesrc-build had happened to be running in. This should
not be a user-visible change as the snapshot is unlinked when kdesrc-build is
done with it anyways.
</p></li>

<li><p>The git snapshot archive requires a specific script to be run to setup
the tarball, which is not compatible with all BSDs. This release works around
the problem by using POSIX sh to run the initialization script.
</p></li>

<li><p>The --no-snapshots option will actually keep git snapshots from being
used this time.
</p></li>

<li><p>Fix a bug with custom source directories introduced with 1.14.
</p></li>

<li><p>Various fixes/updates to the sample configuration file, including the
addition of the kactivities library factored out of kdelibs.
</p></li>

<?php
  include("footer.inc");
?>
