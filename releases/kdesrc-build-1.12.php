<?php
  $release_version = '1.12';
  $release_title = "kdesrc-build $release_version";
  $release_file = "kdesrc-build-$release_version.tar.bz2";
  $page_title = "kdesrc-build release $release_version";
  $site_root = "../";

  include("header.inc");
?>

<p>Released: <b>2010-Jun-26</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?>

<?php if(file_exists("$release_file.asc")) {
  echo "<br>Code signature: <a href=\"$release_file.asc\">PGP signature</a> - key id 0x7B6AE9F2";
} ?>
</p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.11.php">1.11</a>):</p>

<h3>New features/changes:</h3>
<ul>
<li>Changed the name of the script from kdesvn-build to kdesrc-build to account for the ability to build from git as well as svn.</li>
<li>Add a feature to uninstall a module. This implementation relies on CMake support for the feature. You can use the "use-clean-install" option to make kdesrc-build uninstall a module before it installs it. This feature is still considered experimental. (<a href="http://bugs.kde.org/show_bug.cgi?id=211745">Bug 211745</a>)</li>
<li>kdesrc-build will show the file name of the rc file that is needed to edit when informing the user that no rc file exists.</li>
<li>The git-clone command now outputs to stderr. This helps prevent errors on the gitorious.org side when checking out Qt, and also allows for a measure of progress output.</li>
<li>Now the source-dir option can be overridden on a per-module basis, with a patch provided by Michael Hansen. (<a href="http://bugs.kde.org/show_bug.cgi?id=220242">Bug 220242</a>)</li>
<li>Enable support for the ionice command, available on some distrubutions that use Linux >2.6.13. See the use-idle-io-priority command. (<a href="http://bugs.kde.org/show_bug.cgi?id=194635">Bug 194635</a>)</li>
<li>Add the --resume-after option, which is similar to --resume-from, and intended for the case where the user corrects a module failure manually. This option will cause kdesrc-build to continue the build after the selected module. Feature contributed by Tom Albers.</li>
<li>Prefer gmake to make if gmake is present. This allows some modules to build on BSD that use GNU-specific make options. (<a href="http://bugs.kde.org/show_bug.cgi?id=193070">Bug 193070</a>)</li>
<li>Some kdesrc-buildrc-sample cleanups.</li>
</ul>

<h3>Bugfixes:</h3>
<ul>
<li>Correct the path used for building stable l10n modules. (<a href="http://bugs.kde.org/show_bug.cgi?id=201329">Bug 201329</a>)</li>
<li>Place log files in the appropriate directory when using a module name of the form module/foo/bar at the command line. (<a href="http://bugs.kde.org/show_bug.cgi?id=210213">Bug 210213</a>)</li>
<li>Fixed --resume by symlinking the latest build-status file. (<a href="http://bugs.kde.org/show_bug.cgi?id=219386">Bug 219386</a>)</li>
<li>Fixed the default location for kdevplatform and kdevelop.</li>
<li>Fixed a bug where global set-envs were not reset for every module. (<a href="http://bugs.kde.org/show_bug.cgi?id=217638">Bug 217638</a>)</li>
<li>Made all module-specific options that set where the checkout occurs from get a chance to process before kicking up to global options. This allows a module's module-base-path to override a global branch setting for instance. (<a href="http://bugs.kde.org/show_bug.cgi?id=223341">Bug 223341</a>)</li>
<li>Add more sanity checking to the rc file option reading. (<a href="http://bugs.kde.org/show_bug.cgi?id=187841">Bug 187841</a>)</li>
</ul>

<?php
  include("footer.inc");
?>
