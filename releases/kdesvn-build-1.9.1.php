<?php

  $release_version = '1.9.1';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2009-Jun-05</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?>

<?php if(file_exists("$release_file.asc")) {
  echo "<br>Code signature: <a href=\"$release_file.asc\">PGP signature</a> - key id 0x7B6AE9F2";
} ?>
</p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.9.php">1.9</a>):</p>

<p><b>Note:</b> KDE 3.5-series build support has not been tested in some time, but should still work.
This is <u>the last kdesvn-build release</u> where that will be true, I plan on removing all KDE 3-specific
code before the next release. (This was also true of 1.9, this is a bugfix-only release however).</p>

<h3>Bugfixes:</h3>
<ul>
  <li>Switch kdesvn-build over to a more-officially-supported method of running the qt-copy configure
  script without user intervention.  The old method that was required causes an infinite loop in the
  Qt configure script now.  This can cause your disk to fill up with the configure script output so
  it is a good idea to upgrade before you re-build qt-copy.</li>
</ul>

<?php
  include("footer.inc");
?>
