<?php

  $release_version = '1.6.1';
  $release_title = "kdesvn-build $release_version";
  $release_file = "kdesvn-build-$release_version.tar.bz2";
  $page_title = "kdesvn-build release $release_version";
  $site_root = "../";

  include("kdesrc-build.inc");
  include("header.inc");
?>

<p>Released: <b>2008-Jun-28</b></p>

<p>Download it: <a href="<?php echo $release_file; ?>"><?php echo $release_file; ?></a>
<?php echo niceFileSize($release_file); ?></p>

<p><?php echo $release_title; ?> had the following changes from the prior release (<a href="kdesvn-build-1.6.php">1.6</a>):</p>

<h3>Bugfixes:</h3>
<ul>
 <li>Don't delete the build directory for a module if it's also the source directory somehow.</li>
 <li>Give more accurate log file for module update failures.</li>
 <li><p>Add support for a made-up module called <b>phonon</b>.  This allows kdesvn-build to build the
     recommended Phonon 4.2 version which is maintained as a branch.</p>

     <p>If you want to build Phonon 4.2 in this way you need to change the following in your kdesvn-buildrc:</p>

     <ol>
       <li><p>Add something like the following to your kdesvn-buildrc after qt-copy (if you use it) and before
         kdelibs:</p>

         <code>module phonon<br>
         &nbsp;&nbsp;branch 4.2<br>
         end module</code>
       </li>
       <li>If you build qt-copy, add <tt>-no-phonon</tt> to its configure-flags to prevent it from building
         its own Phonon.</li>
       <li>If you build kdesupport, add <tt>-DBUILD_WITH_phonon=OFF</tt> to its cmake-options to prevent it
         from building the trunk version of Phonon.</li>
     </ol>
 </li>
 <li>Fix the path used for searching for Qt's pkg-config files which could interfere with CMake checks for
   some modules.</li>
 <li>Don't symlink the build directory to the source directory for CMake-using modules which required
   symlinking in KDE 3.</li>
 <li>Update kdesvn-buildrc-sample file for new Phonon handling.</li>
</ul>

<?php
  include("footer.inc");
?>
